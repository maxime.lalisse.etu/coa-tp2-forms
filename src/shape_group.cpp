#include "shape_group.hpp"

ShapeGroup::ShapeGroup(vector<Shape*> &shapes) : Shape(0,0),
    xxx(0), yyy(0) {

    for (auto shape : shapes) {
        this->shapes.push_back(shape);
    }
}

void ShapeGroup::draw(Image &img)
{
    cout << "ShapeGroup::draw() called" << endl;
    for (auto shape: shapes) {
        shape->draw(img);
    }
}

void ShapeGroup::move(int x, int y)
{
    for (auto shape: shapes) {
        shape->move(shape->xx + (x - xxx), shape->yy + (y - yyy));
    }
}


bool ShapeGroup::is_inside(int x, int y) const
{
    for (auto shape: shapes) {
        if (shape->is_inside(x, y)) {
            return true;
        }
    }
    return false;
}

Shape::select_state ShapeGroup::select()
{
    select_state state = Shape::select();

    xxx = x;
    yyy = y;

    for (auto shape: shapes) {
        shape->selected = selected;
    }

    return state;
}


ShapeGroup::~ShapeGroup() {
    for (auto shape: shapes) {
        delete shape;
    }
}
