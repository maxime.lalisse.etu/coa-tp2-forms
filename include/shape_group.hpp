#ifndef __SHAPE_GROUP_H__
#define __SHAPE_GROUP_H__

#include <vector>
#include "shape.hpp"

class ShapeGroup : public Shape {
    int xxx, yyy;
    friend class Shape;
    vector<Shape *> shapes;
public:
    ShapeGroup(vector<Shape*> &shapes);

    void move(int x, int y);
    void draw(Image &img);
    bool is_inside(int x, int y) const;
    Shape::select_state select();

    ~ShapeGroup();
};

#endif
